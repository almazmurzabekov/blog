---
author: "Almaz Murzabekov"
title: "[Podcast] Talk about Data Catalogs"
date: "2022-10-14"
description: "I’m excited to share my first-ever talk on podcast 🎉"
tags: ["podcast", "data", "data coffee", "catalogs"]
ShowToc: false
ShowBreadCrumbs: true
---

Right now, we're building a company-wide place for managing the company's data assets. It should become a single workspace where engineers, data scientists, data analytics, products, and managers can cooperate on top of the data assets.

Several weeks ago I was invited to have a conversation about modern data stacks and Data Catalogs particularly. So, here it is, the podcast. Hope you will find it useful, otherwise please do not waste your time :)

### `Please note, podcast in Russian`

{{< youtube z3GcqpoM5p4 >}}



