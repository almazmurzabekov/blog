---
title: "Bookshelf"
layout: "bookshelf"
summary: "The list of books I read/going to read with the short summary"
---

### Books list for 2024

- [Designing Data-Intensive Applications](https://www.amazon.co.uk/dp/1449373321)
- [The Staff Engineer's Path: A Guide For Individual Contributors Navigating Growth and Change](https://www.amazon.co.uk/dp/1098118731)
- [Infrastructure as Code](https://www.amazon.co.uk/dp/1491924357)
- [Design It!: From Programmer to Software Architect](https://www.amazon.co.uk/dp/1680502093)
- [Release It! Design and Deploy Production–Ready Software](https://www.amazon.co.uk/dp/1680502395)
- [Building Evolutionary Architectures: Automated Software Governance](https://www.amazon.co.uk/dp/1492097543/)
- [Software Architecture: The Hard Parts: Modern Trade-Off Analyses for Distributed Architectures](https://www.amazon.co.uk/dp/1492086894)
- [Distributed Systems](https://www.amazon.co.uk/dp/9081540637)


