---
title: "About"
layout: "me"
---

## Me 👨‍💻

{{< css.inline >}}
<style>
img {
	width: 400px
}
</style>
{{< /css.inline >}}

{{< figure src="/images/almaz.png" title="" alt="Almaz Murzabekov">}}

Hi 👋, my name is Almaz and I am a Data Engineer with more than 8 years of experience. I have always been passionate about high-load and big-data systems and have been fortunate to be able to pursue my interests through my work and personal projects. In my free time, you can find me learning about new things that spark my curiosity. I am excited to share my passions and experiences with you, and I hope that you will join me on my journey of discovery and learning.

### Specialities
- Data Engineering, Data Infrastracture, Analytics Engineering
- Databricks (Spark SQL, Spark Streaming, Delta Lake House)
- AWS (S3, EC2, EMR, Hive),
- Atlan, Apache Atlas, (Open Metadata)
- Kafka, Airflow, dbt, docker, k8s
- Python, SQL, Java, Scala, Rust

### Education

2019-2021: MSc in [Big Data Systems](https://www.hse.ru/en/ma/bigdata/), Higher School of Economy, Moscow

2011-2015: BSc in Applied Informatics, Saratov State Technical University, Moscow