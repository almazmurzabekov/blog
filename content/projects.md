---
title: "Projects"
layout: "projects"
---

Hello everyone, welcome to my personal project showcase! 

I'm excited to share with you some of the projects that I have been working on in my spare time. These projects represent my interests, passions, and areas of focus, and I have enjoyed the opportunity to explore and learn new things while working on them. 

In this showcase, you'll get a glimpse into the projects that I have been working on and some of the things that I have learned along the way. 

I have an ambitious project called the `Implementing X in Rust Series`, which consists of a series of books focused on implementing specific technologies using the Rust programming language. This project requires a lot of effort and dedication, but I am confident that the end result will be worth it.

- [Implementing BitTorrent Client in Rust](https://almazmurzabekov.gitlab.io/implementing-bittorrent-client-in-rust/) [Work in Progress]. Upcoming milestone: Draft version of the book in April 2023
- Implementing Bitcoin in Rust [Planned]. 
- Implementing HDFS in Rust [Planned]. 
- Implementing Cassandra in Rust [Planned]. 
- Implementing IPFS in Rust [Considering].